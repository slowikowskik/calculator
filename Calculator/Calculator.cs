﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Calculator
{
    public class Calculator
    {
        public Calculator() { }
        public int long_calculate(string[] numbers)
        {
            int sum = 0;
            for (int i = 0; i < numbers.Length; i++)
            {
                if (int.Parse(numbers[i]) < 0)
                    throw new ArgumentException("Negative number");
                if (int.Parse(numbers[i]) <= 1000)
                    sum += int.Parse(numbers[i]);
            }
            return sum;
        }
        public int new_separator(string[] numbers)
        {
            string new_sep = numbers[0].Substring(2);
            List<string> separators_param = new List<string>();
            if (new_sep.StartsWith('['))
            {
                string[] seprators = new_sep.Split(']');
                for(int i=0; i < seprators.Length-1; i++)
                {
                    new_sep = seprators[i].Substring(1, seprators[i].Length - 1);
                    separators_param.Add(new_sep);
                }
            }
            else
                separators_param.Add(new_sep);
            return long_calculate(numbers[1].Split(separators_param.ToArray(), new StringSplitOptions()));
        }
        public int calculate(string number_string)
        {
            string[] numbers;
            if (number_string=="")
                return 0;
            if (number_string.StartsWith("//"))
            {
                numbers = number_string.Split('\n');
                return new_separator(numbers);
            }
                
            else
                numbers = number_string.Split('\n',',');
            if (numbers.Length == 1)
                return int.Parse(numbers[0]);            
            if (numbers.Length >=2)
            {              
                    
                return long_calculate(numbers);
            }
            return -1;
        }
    }
}
