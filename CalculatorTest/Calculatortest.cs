using NUnit.Framework;
using Calculator;
using System;

namespace Tests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void test_zero()
        {
            var calculator = new Calculator.Calculator(); 
            //Given
            var empty_string = "";
            var expected_result = 0;

            //When
            var result = calculator.calculate(empty_string);

            //Then
            Assert.AreEqual(result, expected_result);
        }
        [Test]
        public void test_number()
        {
            var calculator = new Calculator.Calculator();
            //Given
            var number_string = "4";
            var expected_result = int.Parse(number_string);

            //When
            var result = calculator.calculate(number_string);

            //Then
            Assert.AreEqual(result, expected_result);
        }
        [Test]
        public void test_add()
        {
            var calculator = new Calculator.Calculator();
            //Given
            var number_string = "4\n2";
            var expected_result = 6;

            //When
            var result = calculator.calculate(number_string);

            //Then
            Assert.AreEqual(result, expected_result);
        }
        [Test]
        public void test_add2()
        {
            var calculator = new Calculator.Calculator();
            //Given
            var number_string = "4\n3,14";
            var expected_result = 21;

            //When
            var result = calculator.calculate(number_string);

            //Then
            Assert.AreEqual(result, expected_result);
        }
        [Test]
        public void test_exception()
        {
            var calculator = new Calculator.Calculator();
            //Given
            var number_string = "4\n-3,14";
            Assert.That(() => calculator.calculate(number_string), Throws.TypeOf<ArgumentException>());
        }
        [Test]
        public void test_higher_1000()
        {
            var calculator = new Calculator.Calculator();
            //Given
            var number_string = "4\n1002,14";
            var expected_result = 18;

            //When
            var result = calculator.calculate(number_string);

            //Then
            Assert.AreEqual(result, expected_result);
        }
        [Test]
        public void test_new_separator()
        {
            var calculator = new Calculator.Calculator();
            //Given
            var number_string = "//#\n4#5#6";
            var expected_result = 15;

            //When
            var result = calculator.calculate(number_string);

            //Then
            Assert.AreEqual(result, expected_result);
        }
        [Test]
        public void test_new_separator_with_brakets()
        {
            var calculator = new Calculator.Calculator();
            //Given
            var number_string = "//[##]\n4##5##6";
            var expected_result = 15;

            //When
            var result = calculator.calculate(number_string);

            //Then
            Assert.AreEqual(result, expected_result);
        }
        [Test]
        public void test_new_separators_with_brakets()
        {
            var calculator = new Calculator.Calculator();
            //Given
            var number_string = "//[##][,][???]\n4##5,6???1";
            var expected_result = 16;

            //When
            var result = calculator.calculate(number_string);

            //Then
            Assert.AreEqual(result, expected_result);
        }
    }
}